FROM python:3.8-slim

WORKDIR /app

ENV VERSION = 1
ENV API_KEY = "8fd2281b330c5080e98a03ffa265be28"
#ENV SECRET_KEY = ""
ENV ALLOWED_HOSTS = "localhost 127.0.0.1 0.0.0.0 *"
#ENV DEBUG = 0

COPY Pipfile .
COPY Pipfile.lock .
COPY WeatherSound ./WeatherSound

RUN pip install --no-cache-dir pipenv==2021.11.23 && \
    pipenv install --system --deploy --ignore-pipfile && \
    python WeatherSound/manage.py collectstatic --no-input &&\
    pipenv --clear && \
    rm Pipfile Pipfile.lock

WORKDIR /app/WeatherSound

CMD ["gunicorn", "WeatherSound.wsgi", "--bind", "0.0.0.0:8000"]
