all: set-test-env

set-test-env:
	@python -m venv bandit-env
	@if  [ ! -e reports/ ]; then mkdir reports; fi

bandit-test:
	@if  [ ! -e reports/bandit.json ]; then touch reports/bandit.json; fi
	@python -m pip install bandit
	@bandit -r WeatherSound/ --output reports/bandit.json

safety-check:
	@if  [ ! -e reports/safety.json ]; then touch reports/safety.json; fi
	@python -m pip install --upgrade pip
	@python -m pip install safety
	@safety check --full-report --json --output reports/safety.json 

flake:
	@python -m pip install flake8
	@flake8 --max-line-length 120 --max-complexity 10 WeatherSound/ --count --output-file reports/flake8

hadolint:
	@hadolint -f gitlab_codeclimate docker-compose.yaml >> reports/hadolint
	@hadolint -f gitlab_codeclimate Dockerfile >> reports/hadolint

compose:
	@docker-compose up --detach

clean:
	@docker rmi backend:latest frontend:latest
