from django.contrib import admin
from django.urls import path, include
import os

urlpatterns = [
    path('admin/', admin.site.urls),
    path(f"api/v1/", include('api.urls'))
    # path(f"api/v{os.getenv('VERSION', default='1')}/", include('api.urls'))
]
