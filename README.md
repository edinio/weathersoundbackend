# WeatherSound API

## Description
Cette API a été développée dans le cadre d'un projet étudiant sur le sujet de la gestion de projet Agile en mode Scrum.  
L'API est la partie _backend_ dudit projet qui contient également une partie _frontend_ et qui a pour but de service la météo (pluie, vent, nuage) et de manière accessible aux malvoyants avec notamment des sons.  
Elle est développée en Python avec le framework Django.

## Les services
 ### [weekly_weather](https://weathersound.fr/api/v1/) :
Ce service fournit les prévisions météo des 7 jours à venir à partir de la journée en cours en prenant comme argument les coordonnées gps lontitude et latitude. Les informations fournis par le service sont :
 * La température actuelle
 * la température minimum
 * la température maximum
 * la température ressentie
 * La vitesse du vent
 * la couverture du ciel (nuageux...)
 * les précipitation
 * la date du jour 
 
l'url de l'API est le suivant [url]( https://weathersound.fr/api/v1/),
cette url prend comme paramètre: latitude, longitude 
  

 ### [current_weather](https://weathersound.fr/api/v1/):
 Ce service API fournit les prévisions météo de la journée courante à partir des coordonnées gps lontitude et latitude, les informations fournis sont :
 * La ville (à partir de l'adresse ip de l'utilisateur)
 * La température actuelle
 * la température minimum
 * la température maximum
 * la température ressentie
 * La vitesse du vent
 * la couverture du ciel (nuageux...)
 * les précipitation
 * la date du jour 
  
l'url de l'API est le suivant [url]( https://weathersound.fr/api/v1/),
cette url prend comme paramètre: latitude, longitude 

### [daily_weather](https://weathersound.fr/api/v1/):
 Ce service fournit les prévisions météo de la journée choisie par l'utilisateur, cette API prend comme paramètre les coordonnées gps lontitude et latitude plus la date sous format YYYY-MM-DD, les informations fournis sont :
 
 * la température minimum
 * la température maximum
 * la température ressentie
 * Les conditions météo (pluie, soleil..)
 * La vitesse du vent
 * la couverture du ciel (nuageux...)
 * les précipitation
 * les températures du : matin, après-midi, soir et nuit.


## Déploiement
L'API est déployable dans un conteneur dont le Dockerfile est fourni dans le projet.  
Pour déployer l'API il faut se placer à la racine du projet (où se trouve le Dockerfile) et exécuter les commandes suivantes:
* Créer un fichier d'environnement  
Ce fichier contient les variables qui permettent de configurer l'API. Il faut le créer et le remplir avec les clefs-valeurs suivantes:  
  * VERSION: version de l'API (0 par défaut)  
  * API_KEY: clef qui permet à notre API d'interroger l'API _OpenWeatherMap_. Il faut créer un compte sur le site d'_OpenWeatherMap_ pour avoir cette clef.
  * CVS_PATH:  
  * ALLOWED_HOSTS: Il s'agit des adresses des machines/sites autoriés à interroger l'API (mettre par ex. _127.0.0.1_ ou _localhost_ si on veut l'interroger à partir de la machine dans laquelle le conteneur tourne).
* Construire l'image  
```bash
docker build -t [my_api_image]:[my_image_version] .
```
Avec:   
  * [my_api_image]: nom de l'image  
  * [my_image_version]: version de l'image (ex.: 1.0).  
* Puis lancer le conteneur:
```bash
docker run -p [host_port]:8000 --name [my_docker_name] --env-file [path/to/env/file] [my_image_name]:[image_version]
```
Cette commande lance le conteneur (et donc le serveur de l'API) et les requêtes vers le port _[host_port]_ du host sont redirigées vers le port _8000_ du conteneur.

## Base de données (liste de 138.000 villes)
La base de données de l'API contient une liste des villes du monde (de 10000 hab). Cette liste a été récupérée via le fichier CSV _api/files/cities.csv_.
Ce fichier contient beaucoup plus d'informations que nécessaire pour l'API. Nous avons jugé plus intéressant de ne récupérer que les données utiles:  
* Nom de ville, 
* Pays, 
* Nombre d'habitants, 
* Longitude et latitude  
   
et de les charger dans la base de données.
Etant donné le nombre important de lignes (138000) et donc le temps nécessaire pour les charger en base de données nous livrons cette version avec la base de données déjà chargée (db.sqlite3).  
Pour charger les données du CSV nous avons exécuté le fichier _api/management/commands/load_cities.py_.

